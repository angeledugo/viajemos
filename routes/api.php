<?php

use Zttp\Zttp;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/weather', function () {
    $apiKey = config('services.openweathermap.key');
    $lat = request('lat');
    $lng = request('lng');

    $response = Zttp::get("https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lng&APPID=6dec67b6f20c85d0f0353c94d19e47c0&units=metric");

    return $response->json();
});
