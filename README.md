<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Weather

esta aplicacion permite consultar el clima asi como tambien sensacion termina de sus ciudades apoyado en el consumo de las siguiente apis

- [openweathermap](https://openweathermap.org).
- [community.algolia.com](https://community.algolia.com).


## Author

Ing. Angel Gonzalez angeledugo@gmail.com (3115344239)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
